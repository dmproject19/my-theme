<?php /*------------------ THEME HEADER -------------------*/ ?> 
<!DOCTYPE html>
<html>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head(); ?>
</head>
<body>
<header class="site-header">
<nav class='site-menu navbar navbar-default'>
    <div class="site-logo">
        <p>logo here</p>
    </div>
    <?php wp_nav_menu(array(
        'theme_location'  => 'primary',
        'depth'           => 2,
        'container_id'    => 'primary-menu',
        'menu_class'      => 'menu clearfix',
    )); ?>
    
</nav>
</header>
<main class="content">
    <p>main here</p>
    <h1>test content</h1>
    <p>
        What is Lorem Ipsum?
Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.

Why do we use it?
It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable
    </p>
